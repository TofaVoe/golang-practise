package main

import (
	"fmt"
	"strconv"
)

const Pi = 3.14

type StringList struct {
	head string
	tail *StringList
}

type Hamburger struct {
	meat        string
	ingredience *Ingredience
	level       int
}

type Ingredience struct {
	name         string
	caloricValue int
}

func listRangePointer(a int, b int) *StringList {

	if a < b {
		strA := strconv.Itoa(a)
		// strB := strconv.Itoa(b)
		return &StringList{head: strA, tail: listRangePointer(a+1, b)}
	} else {
		return nil
	}
}

func listRange(a int, b int) StringList {
	return *listRangePointer(a, b)
}

func printListln(list *StringList) {
	if list != nil {
		fmt.Printf(list.head)
		fmt.Print(", ")
		printListln(list.tail)
	} else {
		fmt.Print("\n")
	}
}

func makeHamburger(i *Ingredience) Hamburger {
	return Hamburger{"beef", i, 1}
}

func sumAll(n int) int {
	var sum int = 0

	for i := 0; i < n; i++ {
		// var x int = 5
		if x := 5; i%2 == 0 {
			sum += i + x
		} else {
			sum -= i - x
		}
	}

	return sum
}

func get(list *StringList, n int) string {
	var value string

	if list == nil {
		return value
	} else {
		if n == 0 {
			return list.head
		} else {
			return get(list.tail, n-1)
		}
	}
}

func main() {
	fmt.Println("Hello World!")

	var x int32 = 10

	var y string = string(x)

	fmt.Printf("y is type of %T\n", y)

	fmt.Print("Result is: ")
	fmt.Println(sumAll(5))
	fmt.Println(Pi)

	var pickle Ingredience = Ingredience{"Pickle", 2}
	var picklePointer *Ingredience = &pickle
	// pickle := Ingredience{"Pickle", 2}
	result := makeHamburger(picklePointer)
	fmt.Println(result)
	fmt.Println(*result.ingredience)

	var list [4]string = [4]string{
		"John",
		"Paul",
		"Rambo",
		"Dutch",
	}

	fmt.Println(list[2])

	list1 := StringList{head: "A"}
	list2 := StringList{head: "B"}
	list1.tail = &list2

	printListln(&list1)

	intList := listRange(10, 20)
	pos := get(&intList, 5)

	printListln(&intList)
	fmt.Println(pos)
}
