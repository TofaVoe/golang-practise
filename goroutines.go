package main

import (
	"fmt"
	"time"
)

func sendHttpRequest(httpResponse chan int, interupt chan int) int {
	// time.Sleep(duration)
	return 1
}

func say(name string, s string, c chan int) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Printf("<%s> ", name)
		fmt.Println(s)
	}
	c <- 0
}

func main() {
	// c := make(chan int)
	// go say("A", "world", c)
	// // say("B", "hello")
	// <-c
	// fmt.Println("Hello")

	// httpResponse0 := make(chan int)
	// httpResponse1 := make(chan int)
	// interupt0 := make(chan int)
	// interupt1 := make(chan int)

	// go sendHttpRequest(httpResponse0, interupt0)
	// go sendHttpRequest(httpResponse1, interupt1)

	// var response int

	// select {
	// case response = <-httpResponse0:
	// 	interupt1 <- 0

	// case response = <-httpResponse1:
	// 	interupt0 <- 0
	// }

	// fmt.Print(response)

	x := 0

	for i := 0; i < 1000; i++ {
		go func() {
			x = x + 1
		}()
	}
	time.Sleep(1000 * time.Millisecond)
	fmt.Println(x)
}
